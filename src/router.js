import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/catalog",
            component: () => import("@/components/content/pages/catalog")
        },
        {
            path: "/",
            redirect: "/hello"
        },
        {
            path: "/profile/:profileUsername",
            component: () => import("@/components/content/pages/profile"),
            props: true,
        },
        {
            path: "/profile",
            component: () => import("@/components/content/pages/profile"),
        },
        {
            path: "/sign_in",
            component: () => import("@/components/auth/sign_in")
        },
        {
            path: "/sign_up",
            component: () => import("@/components/auth/sign_up")
        },
        {
            //TODO: разделение на image и regions
            path: "/quiz/",
            component: () => import("@/components/content/pages/quiz_page"),
            children: [
                {
                    path: "usa_states",
                    component: () => import("@/components/content/maps/usa_states_map")
                },
                {
                    path: "serbia_regions",
                    component: () => import("@/components/content/maps/serbia_regions")
                },
                {
                    path: "italy_regions",
                    component: () => import("@/components/content/maps/italy_regions")
                }
            ]
        },
        {
            path: "/image-quiz/",
            component: () => import("@/components/content/pages/image_quiz"),
            props: (route) => ({api_url: route.name}),
            children: [
                {
                    path: "france-presidents",
                    name: "/123",
                }
            ]
        },
        {
            path: "/info/",
            component: () => import("@/components/content/pages/info/"),
            children: [
                {
                    path: "usa",
                    component: () => import("@/components/content/info/usa_info")
                },
                {
                    path: "serbia",
                    component: () => import("@/components/content/info/serbia_info")
                },
                {
                    path: "italy",
                    component: () => import("@/components/content/info/italy_info")
                }
            ]
        },
        {
            path: "/leaderboard",
            component: () => import("@/components/content/pages/leaderboard")

        },
        {
            path: "/hello",
            component: () => import("@/components/content/pages/hello_page")
        },
        { path: "*", component: () => import("@/components/content/pages/NotFound") }
    ]
});

const DEFAULT_TITLE = 'GeoQuiz';
router.afterEach((to) => {
    Vue.nextTick(() => {
        document.title = to.meta.title || DEFAULT_TITLE;
    });
});