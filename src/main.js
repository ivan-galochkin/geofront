import Vue from 'vue'
import App from './App.vue'
import {router} from "@/router";
import {BootstrapVue} from 'bootstrap-vue'

import axios from "axios";
import VueAxios from "vue-axios";
import Vuex from "vuex"
import createPersistedState from 'vuex-persistedstate'
import VueKinesis from "vue-kinesis";

Vue.config.productionTip = false

Vue.use(VueKinesis);
Vue.use(BootstrapVue);
Vue.use(VueAxios);
Vue.use(Vuex);



const defaultState = () => {
    return {
        tokens: {access_token: null, refresh_token: null},
        user: null,
        quizData: {
            current_state: null,
            total_time: null,
            total_points: null,
            token: "123"
        },
        additionalData: {
            mapVisible: true,
            api_host: "https://api.earth-quiz.uk", //TODO localhost
        }
    }
}

export const store = new Vuex.Store({
    plugins: [createPersistedState({
        paths: ["tokens", "user"],
        storage: window.localStorage,
    })],
    state: defaultState(),
    mutations: {
        setTokens(state, payload) {
            state.tokens.access_token = payload.tokens.access_token;
            state.tokens.refresh_token = payload.tokens.refresh_token;
        },
        setUser(state, payload) {
            state.user = payload.user;
        },
        setCurrentState(state, payload) {
            state.quizData.current_state = payload.current_state;
        },
        deleteCredentials(state) {
            Object.assign(state, defaultState());
            localStorage.clear();
            router.push('/sign_in');
        },
        hideMap(state) {
            state.additionalData.mapVisible = false;
        },
        showAlert(state) {
            state.additionalData.alertVisible = true;
        },
        hideAlert(state) {
            state.additionalData.alertVisible = false;
        }
    },
    getters: {
        isLoggedIn(state) {
            return !!state.tokens.access_token;
        },
        getLengthStates(state) {
            return state.quiz_data.current_states.length;
        },
        getAccessToken(state) {
            return state.tokens.access_token;
        }
    }

});


new Vue({
    store,
    router,
    axios,
    render: h => h(App),
    data: {
        token: ""
    }
}).$mount('#app')
